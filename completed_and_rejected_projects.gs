var MAIN_SHEET_ID = 'Spreadsheet ID';

//** Main function
function makeReport() {
  //sheet for data manipulation
  var sheetData = SpreadsheetApp.getActiveSpreadsheet().getSheetByName('DATA');
  var ss = SpreadsheetApp.openById(MAIN_SHEET_ID); //Spreadsheet with data
  var sheet = ss.getSheetByName('sheet_in_main_spreadsheet');
  copyData(sheetData, sheet);
}
//** Copy all necessary data from main spreadsheet 'MAIN_SHEET_NAME' 
function copyData(sheetData, sheet) {
  sheetData.getDataRange().clearContent();
  var product = ['product_01',
                 'product_02',
                 'product_03',
                 'product_04',
                 'product_05',
                 'product_06',
                 'product_07',
                 'product_08',
                 'product_09',
                 'product_10',
                 'product_11',
                 'product_12'];
  var values = sheet.getRange('A:Y').getValues();
  var data = [];    
  for(var i = 0; i < values.length; i++){
    for(var j = 0; j < product.length; j++){
      if(values[i][8] == product[j]) {
        data.push(values[i]);
      }
    }
  }sheetData.getRange(2, 1, data.length, data[0].length).setValues(data);
  //(!)delete unnecessary columns  
  sheetData.deleteColumns(2, 7);
  sheetData.deleteColumns(3, 11);
  sheetData.deleteColumns(4, 3);
  sheetData.insertColumnsAfter(sheetData.getLastColumn(), 6);
  countAll(sheetData);
}

function countAll(sheetData) {
  var date = new Date();
  var dateFromMs = Number(new Date(date.getFullYear(), date.getMonth() - 1, 1).getTime()).toFixed(0); 
  var dateTo = Utilities.formatDate(new Date(date.getFullYear(), date.getMonth(), 1), "GMT", "yyyy-MM-dd");
  var dateToMs = Number(new Date(date.getFullYear(), date.getMonth(), 1).getTime()).toFixed(0);
  sheetData.getRange(2, 5).setValue(dateTo);
  var values = sheetData.getRange(2, 1, sheetData.getLastRow(), 1).getValues();
  var count = 0;
  for(var i = 0; i < values.length; i++){
    var cellValue = new Date(values[i][0]);
    var msData = Number(cellValue.getTime()).toFixed(0);
    if(msData >= dateFromMs && msData < dateToMs){
      count++;
    }
  }sheetData.getRange(2, 6).setValue(Number(count).toFixed(0));
  countSuccess(sheetData);
}

function countSuccess(sheetData) {
  var date = new Date();
  var dateFromMs = Number(new Date(date.getFullYear(), date.getMonth() - 1, 1).getTime()).toFixed(0); 
  var dateToMs = Number(new Date(date.getFullYear(), date.getMonth(), 1).getTime()).toFixed(0);
  var values = sheetData.getRange(2, 3, sheetData.getLastRow(), 2).getValues();
  var count = 0;
  for(var i = 0; i < values.length; i++){
    if(values[i][0] == 'status_complete'){
      var cellValue = new Date(values[i][1]);
      var msData = Number(cellValue.getTime()).toFixed(0);
      if(msData >= dateFromMs && msData < dateToMs){
        count++;
      }
    }    
  }sheetData.getRange(2, 7).setValue(Number(count).toFixed(0));
  countCancelled(sheetData);
}

function countCancelled(sheetData) {
  var date = new Date();
  var dateFromMs = Number(new Date(date.getFullYear(), date.getMonth() - 1, 1).getTime()).toFixed(0); 
  var dateToMs = Number(new Date(date.getFullYear(), date.getMonth(), 1).getTime()).toFixed(0);
  var values = sheetData.getRange(2, 3, sheetData.getLastRow(), 2).getValues();
  var status = ['status_reject_01',
                'status_reject_02',
                'status_reject_03',
                'status_reject_04'];
  var count = 0;
  for(var i = 0; i < values.length; i++){
    for(var j = 0; j < status.length; j++){
      if(values[i][0] == status[j]){
        var cellValue = new Date(values[i][1]);
        var msData = Number(cellValue.getTime()).toFixed(0);
        if(msData >= dateFromMs && msData < dateToMs){
          count++;
        }
      } 
    }
  }sheetData.getRange(2, 9).setValue(Number(count).toFixed(0));
  colculationPart(sheetData);
}

function colculationPart(sheetData) {
  var all = sheetData.getRange(2, 6).getValue();
  var success = sheetData.getRange(2, 7).getValue();
  var cancelled = sheetData.getRange(2, 9).getValue();
  sheetData.getRange(2, 8).setValue((Number((success / all) * 100).toFixed(2)).toString().replace(".", ","));
  sheetData.getRange(2, 10).setValue((Number((cancelled / all) * 100).toFixed(2)).toString().replace(".", ","));
  logIfSuccess(sheetData);
}

function copyTo(sheetData) {
  var activeSheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName('main_sheet_in_active_Spreadsheet');
  var sourceRange = sheetData.getRange(2, 5, 1, 6);
  var targetRange = activeSheet.getRange(activeSheet.getLastRow() + 1, 1, 1, 6);
  targetRange.setValues(sourceRange.getValues());
  sourceRange.copyTo(targetRange, {contentsOnly:true});
  return true;
}

function logIfSuccess(sheetData) {
  var sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName('sendEmails');
  var status = copyTo(sheetData);
  if(status) {
    sheet.getRange(1, 3, sheet.getLastRow(), 1).setValue('SUCCESS');
  }else{
    sheet.getRange(1, 3, sheet.getLastRow(), 1).setValue('ERROR');
  }sheet.getRange(1, 4, sheet.getLastRow(), 1).setValue(new Date());
}

function sendEmails() {
  var sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName('sendEmails');
  var dataRange = sheet.getRange(1, 1, sheet.getLastRow(), sheet.getLastColumn());
  var data = dataRange.getValues();
  for (i in data) {
    var row = data[i];
    var emailAddress = row[0]; // First column
    var dateTimeGMT = (Utilities.formatDate(row[3], "GMT+2", "dd-MM-yyyy HH:mm:ss"));//row[3];
    var message = row[1] + "\nReport status: " + row[2] + "\nAt: " + dateTimeGMT; //row[3] - third column. row[2] - second column, row[4] - fourth column
    var subject = 'main_sheet's_name_in_active_Spreadsheet';
    MailApp.sendEmail(emailAddress, subject, message);
  }
}